# Corrigés du premier semestre de géométrie MA-BA1 EPFL

Vous trouverez ici tous les corrigés des séries de géométries, rédigés par les élèves, en attendant les corrections officielles. La majorité des corrigés ont été relus par les assistants.

Notez que ceci est un travail collaboratif, alors n'hésitez pas à faire des suggestions, à faire des commits avec vos propres résulats et surtout de relever les potentielles erreurs.

### Corrigés disponibles

- [Corrigé 1 (complet)](serie1/corrige.pdf)
- Corrigé 2
  - [Exo 3](serie2/exo3.pdf)
- [Corrigé 3 (complet)](serie3/corrige3.pdf)

### Ajouter un exo relu

Pour ajouter un exercice que tu as fait relire par un assistant, tu peux:
 - Me l'envoyer à `diego.dorn at epfl.ch`
 - Si tu sais utiliser git, fais une pull request, c'est vraiment plus simple pour moi

### Corriger une erreur

Si par hasard il reste une / des erreurs dans les exos, le plus simple est d'ouvrir une [issue sur gitlab]().
Ouvrir une issue a plusieurs avantages:
 - Tout le monde peut voir le problème / erreur tant qu'il existe
 - Tout le monde peut corriger l'erreur
 - Il existe une liste de toutes les erreurs non corrigées
 - Je ressois pas 3 fois le même mail

Un autre solution est de la corriger toi même. Pour cela, il suis la même procédure que pour ajouter un exercice, mais à la place, change ce qui ne vas pas ;-)
